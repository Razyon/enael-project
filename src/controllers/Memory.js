import Animal from '../classes/Animal'
import Events from '../classes/Events'

const NUMBER_OF_CARDS = 6;

var flippedAnimal = null;
var blockEvents = true;
var animals;

/**
 * Shuffle an array
 */
function shuffle (o) {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {
        ;
    }
    return o;
}

function generateAnimalCard () {
    var node = document.createElement('div');
    node.className = 'card flip';
    node.innerHTML = '<div class="flip"><div class="face back"><div class="face-content"></div></div><div class="face front"><div class="face-content animal"></div></div></div>';
    return node;
}

function generateAnimal (animalType) {
    var cardNode = generateAnimalCard();

    var animal = new Animal(cardNode.querySelector('.animal'), {
        animate: true,
        animalName: animalType
    });

    return {
        card: cardNode,
        animal: animal,
        type: animalType
    };
}

function cardToBack (animal) {
    animal.card.classList.remove('active');
}

function checkIfWin () {
    for (var i = 0; i < animals.length; i += 1) {
        if (!animals[i].blocked) {
            return false;
        }
    }

    resetGame();
}

function checkIfSame (animal1, animal2) {
    if (animal1.type === animal2.type) {
        flippedAnimal = null;
        animal1.blocked = true;
        animal2.blocked = true;
        blockEvents = false;
        checkIfWin();
    } else {
        cardToBack(animal1);
        cardToBack(animal2);
        flippedAnimal = null;
        blockEvents = false;
    }
}

function connectAnimal (animal) {
    var animalToggle = true;

    function onAnimalEnter () {
        if (blockEvents) {
            return;
        }
        if (animal.blocked) {
            return;
        }


        if (flippedAnimal === animal) {
            return;
        }
        if (!flippedAnimal) {
            flippedAnimal = animal;
            animal.card.classList.add('active');
        } else {
            animal.card.classList.add('active');
            blockEvents = true;

            setTimeout(function () {
                checkIfSame(flippedAnimal, animal);
            }, 1000);
        }
    }

    Events.connectNode(
        animal.animal.node,
        {
            onEnter: onAnimalEnter,
            onDragStart: onAnimalEnter,
            onLeave: function () {
                console.log('onLeave');
            }
        }
    );
}

function resetGame () {
    var mainContainer = document.getElementById('mainContainer');
    mainContainer.innerHTML = '';

    var cardGameNode = document.createElement('card-game');
    cardGameNode.className = 'card-game';
    mainContainer.appendChild(cardGameNode);

    var animalList = [].concat(Animal.ANIMAL_LIST);
    shuffle(animalList);
    animalList = animalList.slice(0, 3);

    animals = [];

    for (var i = 0; i < NUMBER_OF_CARDS; i += 1) {
        var animalTypeIndex = Math.max(Math.floor((i / 2)), 0);
        var animal = generateAnimal(animalList[animalTypeIndex]);
        animals.push(animal);
    }

    shuffle(animals);
    for (var i = 0; i < animals.length; i += 1) {
        cardGameNode.appendChild(animals[i].card);
    }

    setTimeout(function () {
        for (var i = 0; i < animals.length; i += 1) {
            console.log('test', animals[i].animal.node);
            connectAnimal(animals[i]);
            blockEvents = false;
        }
    }, 1)
}

export default function controller_memory () {
    Events.setBlockTouch(true);
    resetGame();
};
