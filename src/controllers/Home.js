import Events from '../classes/Events'

var template = `
    <img class="background" src="images/background.svg" />
    <div class="menu" id="menu1">
        <a href="#/memory">memory</a>
    </div>
    <div class="menu" id="menu2">
        <a href="#/shapes">shapes</a>
    </div>
    <div class="menu" id="menu3">
        <a href="#/seek">Seek</a>
    </div>
`;

export default {
    on: function () {
        console.log('controller_home');
        var mainContainer = document.getElementById('mainContainer');

        mainContainer.innerHTML = template;

        Events.setBlockTouch(false);
    },
    after: function () {
        console.log('home leave');
    }
};