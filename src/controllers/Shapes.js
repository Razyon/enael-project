import Events from '../classes/Events'
import Shape from '../classes/Shape'

/**
 * Shuffle an array
 */
function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

var template = '<div class="shape-generator"></div><div class="shape-container"></div>';

var shapes = [];
var shapeStack = [];
var movingShape = null;
var movingShapeIndex = 0;
var movingShapeContainer;

function connectContainer (shape) {
    Events.connectNode(
        shape.domNode,
        {
            onDragOver: function () {
                console.log('onDragOver');

                movingShape.addGradient('red');
            },
            onDragLeave: function () {
                console.log('onDragLeave');

                movingShape.addGradient('orange');
            },
            onDropOver: function () {
                console.log('onDropOver');
                onMovingShapeDropOver(shape);
            }
        }
    );
}

function createShape (shapeName, customClass, color) {
    console.log('createShape', shapeName, color);

    var node = document.createElement('div');
    var shapeBox = document.createElement('div');
    shapeBox.classList.add('shape-box');
    var shape = new Shape(node, {
        shape: shapeName,
        class: customClass,
        color: color
    });

    shapeBox.appendChild(node);

    return shape;
}

function createMovingShape (shapeId) {
    movingShape = createShape(shapeStack[shapeId].shape, 'moving', shapeStack[shapeId].color);

    if (!movingShapeContainer) {
        movingShapeContainer = document.getElementById('mainContainer');
    }
    movingShapeContainer.querySelector('.shape-generator').appendChild(movingShape.domNode.parentNode);

    var startX, startY;
    Events.connectNode(
        movingShape.domNode,
        {
            onEnter: function () {

            },
            onDragStart: function (event) {
                var size = movingShape.domNode.getBoundingClientRect(),
                    containerSize = movingShape.domNode.parentNode.getBoundingClientRect();

                startX = event.clientX - (size.left - containerSize.left);
                startY = event.clientY - (size.top - containerSize.top);

                movingShape.addGradient('blue');
            },
            onDrag: function (event) {
                //console.log('onDrag', event.clientX, event.clientY);
                movingShape.domNode.style.left = event.clientX - startX;
                movingShape.domNode.style.top = event.clientY - startY;
                movingShape.domNode.style.position = 'absolute';
                movingShape.domNode.style.zIndex = 99;

                //movingShape.addGradient('pink');
            },
            onDragEnd: function () {
                console.log('dragEnd');

                movingShape.addGradient('pink');
            }
        }
    );
}

function onMovingShapeDropOver (shape) {
    if (movingShape.shapeName === shape.shapeName) {
        Events.disconnectNode(movingShape.domNode);
        Events.disconnectNode(shape.domNode);

        var movingShapeContainer = movingShape.domNode.parentNode;
        shape.domNode.parentNode.appendChild(movingShape.domNode);

        movingShapeContainer.parentNode.removeChild(movingShapeContainer);

        movingShape.domNode.style.left = 0;
        movingShape.domNode.style.top = 0;
        movingShape.domNode.style.zIndex = 1;

        if (movingShapeIndex === 3) {
            console.log('finished');
            initiateGame();
        } else {
            createMovingShape(++movingShapeIndex);
        }
    }
}

function connectContainers (containers) {
    for (var i = 0; i < containers.length; i += 1) {
        connectContainer(containers[i]);
    }
}

function initiateGame () {
    var container = document.getElementById('mainContainer');
    var shapeContainer = container.querySelector('.shape-container');

    var selectedColors = shuffle(Shape.COLORS).slice(0,4);
    var selectedShapes = shuffle(Shape.SHAPES).slice(0,4);
    var shapeList = [];
    for (var i = 0; i < 4; i += 1) {
        shapeList.push({
            shape: selectedShapes[i],
            color: selectedColors[i]
        });
    }

    shapeStack = shuffle(shapeList);

    for (var i = 0; i < shapes.length; i += 1) {
        shapes[i].domNode.parentNode.parentNode.removeChild(shapes[i].domNode.parentNode);
    }

    shapes = [];
    for (var i = 0; i < 4; i += 1) {
        var shape = createShape(selectedShapes[i]);
        shapes.push(shape);
        shapeContainer.appendChild(shape.domNode.parentNode);
    }

    connectContainers(shapes);

    movingShapeIndex = 0;
    createMovingShape(movingShapeIndex);
}

export default function controller_shapes () {
    var container = document.getElementById('mainContainer');
    container.innerHTML = template;

    Events.setBlockTouch(true);
    initiateGame();
};