import Events from '../classes/Events'

var template = `
    <img class="background" src="images/background.svg" />
    <div class="seek-field-container" id="seekContainer></div>
`;

export default {
    on: function () {
        console.log('controller_home');
        var mainContainer = document.getElementById('mainContainer');

        mainContainer.innerHTML = template;

        Events.setBlockTouch(false);
    },
    after: function () {
        console.log('home leave');
    }
};