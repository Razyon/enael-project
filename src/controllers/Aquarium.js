var template = '<img class="background" src="images/background-aquarium.svg" />';

const controller_aquarium = function () {
    var container = document.getElementById('mainContainer');
    container.innerHTML = template;

    var fishs = [];
    for (var i = 0; i < 10; i += 1) {
        var node = document.createElement('div');

        container.appendChild(node);

        fishs.push(new Fish(node, {
            animalName: 'fish',
            animate: false
        }));
    }

    function step(timestamp) {
        for (var i = 0; i < fishs.length; i += 1) {
            fishs[i].tickUpdate();
        }
        //requestAnimationFrame(step);
    }

    setInterval(step, 1000/30);
    //requestAnimationFrame(step);

};

export default controller_aquarium