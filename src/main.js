'use strict';

import controller_home from './controllers/Home'
import controller_memory from './controllers/Memory'
import controller_aquarium from './controllers/Aquarium'
import controller_shapes from './controllers/Shapes'
import controller_seek from './controllers/Seek'

import Letters from './classes/Letters'
import Events from './classes/Events'

import './main.less'

console.log('This is Main.js');

var pixelInnerHTML = '';
pixelInnerHTML += '<div class="mini-square msq1"><div class="flip"><div class="face front">&nbsp;</div><div class="face back">&nbsp;</div></div></div>';
pixelInnerHTML += '<div class="mini-square msq2"><div class="flip"><div class="face front">&nbsp;</div><div class="face back">&nbsp;</div></div></div>';
pixelInnerHTML += '<div class="mini-square msq3"><div class="flip"><div class="face front">&nbsp;</div><div class="face back">&nbsp;</div></div></div>';
pixelInnerHTML += '<div class="mini-square msq4"><div class="flip"><div class="face front">&nbsp;</div><div class="face back">&nbsp;</div></div></div>';

var letters = new Letters({
  pixelNodeClassName: 'card square',
  letterNodeClassName: 'letter',
  pixelInnerHTML: pixelInnerHTML
});

letters.writeString('enael', document.getElementById('titleContainer'));
Events.setBlockTouch(true);

var routes = {
  '/home': controller_home,
  '/memory': controller_memory,
  '/aquarium': controller_aquarium,
  '/shapes': controller_shapes,
  '/seek': controller_seek
};

var router = Router(routes);

function goToHome() {
  router.setRoute('/home');
}

router.configure({
  notfound: function () {
    console.log('route not found');
    goToHome();
  },
  on: function () {
    document.body.className = router.getRoute();
    console.log('route changed');
  },
  after: function () {
    
  }
});

router.init();

var route = router.getRoute();
if (route[0] === '') {
  goToHome();
}