import SVG from './SVG'

//var animalList = ['bald-eagle', 'bear', 'beaver', 'cat', 'cerf', 'chicken', 'cow', 'dinosaur', 'dog', 'donkey', 'duck', 'elephant', 'elk', 'giraffe', 'goat', 'goose', 'hedgehog', 'hippo', 'kangaroo', 'lion', 'llama', 'pig', 'polar-bear', 'rooster', 'seal', 'sheep', 'squirrel', 'turkey', 'zebra'];  
var animalList = ['cat', 'cow', 'hippo', 'zebra', 'bear', 'pig', 'elephant', 'giraffe', 'sheep'];
var availableAnimations = ['head-shake'];

var getRandomAnimal = function () {
  var number = animalList.length - 1;
  return animalList[Math.round((Math.random() * number) % number)];
};

var getAnimalContent = function (animalFile, callback) {
  var xhr = new XMLHttpRequest();
  
  xhr.open('GET', animalFile);
  xhr.onload = function () {
    callback(xhr.responseText);
  };
  
  xhr.send();
};

/**
 * Animal, set a SVG animal to the specified DOM Node
 * @params {Node} domNode - The node where the animal will be set
 * @params {Object} config - The configuration Object
 * @params {String} config.animalName - (Optional) The name of the animal to set (to be chosen in the animal list)
 * @params {Bool} config.animate - (Optional) Set true if the animal must be animated directly (can be activated afterwards)
 */
var Animal = function (domNode, config) {
  this.config = config;
  if (!this.config) {
    this.config = {};
  }
  
  this.node = domNode;
  this.change(config.animalName);

  console.log('animal', config.animate);
  if (this.config.animate) {
    this.animate();
  }
};

Animal.ANIMAL_LIST = animalList;
Animal.getRandomAnimal = getRandomAnimal;

Animal.prototype.change = function (animalName) {
  if (!animalName) {
    var animalName = getRandomAnimal();
  }
  var self = this;
  
  self.removeAnimation();
  self.node.innerHTML = '';
  self.node.classList.add('animal');

  SVG.getSVG('images/' + animalName + '.svg', function (svg) {
    self.node.appendChild(svg);

    if (self.config.animate) {
      self.animate();
    }
    console.log(animalName);
  });
};

Animal.prototype.animate = function (animationName) {
  if (animationName) {
    this.node.classList.add(animationName);
  } else {
    this.node.classList.add('head-shake');
    this.node.classList.add('breathe');
    this.node.classList.add('eye-blink');
  }
};

Animal.prototype.removeAnimation = function (animationName) {
  if (animationName) {
    this.node.classList.remove(animationName);
  } else {
    this.node.classList.remove('head-shake');
    this.node.classList.remove('breathe');
    this.node.classList.remove('eye-blink');
  }
};


export default Animal;