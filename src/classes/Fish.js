'use strict';

function randomize (min, max) {
    return (Math.round((Math.random() * max)) % (max-min+1)) + min;
}

function getRandomDirection () {
    return {
        x: randomize(-40, 40) / 100,
        y: randomize(-40, 40) / 100
    }
}

function getOppositeDirection (x1, y1, x2, y2) {

}

function getRandomSize () {

}

var Fish = function (domNode, config) {
    this.animal = new Animal(domNode, config);
    this.domNode = domNode;

    this.x = randomize(5, 95);
    this.y = randomize(5, 95);

    this.direction = getRandomDirection();

    console.log('Fish', this.direction);

    this.size = randomize(5, 15);
    domNode.style.width = this.size + 'vmin';
    domNode.style.height = this.size + 'vmin';

    this.animal.animate('tail');
    if (this.direction.x > 0) {
        this.domNode.classList.add('flipped');
    }

    this.move();
};

Fish.prototype.tickUpdate = function () {
    this.x += this.direction.x;
    this.y += this.direction.y;

    if (this.x < 0) {
        this.direction.x = randomize(10, 20) / 100;
        this.domNode.classList.add('flipped');
    }
    if (this.x > 100 - this.size) {
        this.direction.x = randomize(-20, -10) / 100;
        this.domNode.classList.remove('flipped');
    }
    if (this.y < 0) {
        this.direction.y = randomize(10, 20) / 100;
    }
    if (this.y > 100 - this.size) {
        this.direction.y = randomize(-20, -10) / 100;
    }

    this.move();
};

Fish.prototype.move = function () {
    this.domNode.style.top = this.y + '%';
    this.domNode.style.left = this.x + '%';
};

export default Fish;