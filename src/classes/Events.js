'use strict';

var NODE_OBJECT_STATUS = {
        IDLE: 'idle',
        ENTERED: 'entered',
        DRAGGING: 'dragging'
    },
    TOUCH_STATUS = {
        'OVER': 'over',
        'OUT': 'out'
    };

function getNodePosition (node) {
    return node.getBoundingClientRect();
}

function isPointInNode (point, nodeObject, forceRefresh) {
    var nodeBounding = nodeObject.size;
    if (forceRefresh) {
        nodeBounding = getNodePosition(nodeObject.node);
    }
//    var nodeBounding = nodeObject.size;
//        var nodeBounding = getNodePosition(nodeObject.node);
    var pointX = point.clientX,
        pointY = point.clientY;

    if (nodeBounding.left < pointX && nodeBounding.right > pointX && nodeBounding.top < pointY && nodeBounding.bottom > pointY) {
        return true;
    }

    return false;
}

var currentTouches = [];

function copyTouch (touch) {
    return {identifier: touch.identifier, clientX: touch.clientX, clientY: touch.clientY};
}

function getTouchIndexById (idToFind, touches) {
    var _touches = touches || currentTouches;
    for (var i = 0; i < _touches.length; i++) {
        var id = _touches[i].identifier;

        if (id == idToFind) {
            return i;
        }
    }
    return -1;    // not found
}

function getTouchById (idToFind, touches) {
    var index = getTouchIndexById(idToFind, touches);
    if (index >= 0) {
        return touches && touches[index] || currentTouches[index];
    }

    return null;
}

var connectedNodes = [];

var blockingEvents = false;

function blockEvent (evt) {
    if (blockingEvents) {
        evt.stopPropagation();
        evt.preventDefault();
    }
};

var isTouchOver = function (touch, node) {
    for (var i = 0; i < connectedNodes.length; i += 1) {
        if (connectedNodes[i] != node && isPointInNode(touch, connectedNodes[i], true)) {
            return connectedNodes[i];
        }
    }

    return false;
};

var onDragNodeLeave = function (currentNodeObject, nodeObject) {
    if (!nodeObject) {
        return;
    }
    nodeObject.callbacks.onDragLeave && nodeObject.callbacks.onDragLeave();
    currentNodeObject.data.nodeOver = null;
};

var onDragNodeEnter = function (currentNodeObject, nodeObject) {
    nodeObject.callbacks.onDragOver && nodeObject.callbacks.onDragOver();
    currentNodeObject.data.nodeOver = nodeObject;
};

var handleGeneralTouchStart = function (evt) {
    var touches = evt.changedTouches || [evt],
        currentTouch;

    for (var i = 0; i < touches.length; i++) {
        currentTouches.push(copyTouch(touches[i]));
    }
    for (var j = 0; j < connectedNodes.length; j++) {
        var touchStatus = TOUCH_STATUS.OUT;
        var currentNodeObject = connectedNodes[j];
        for (var i = 0; i < currentTouches.length; i++) {
            if (isPointInNode(currentTouches[i], currentNodeObject, true)) {
                touchStatus = TOUCH_STATUS.OVER;
                currentTouch = currentTouches[i];
            }
        }

        switch (currentNodeObject.status) {
            case NODE_OBJECT_STATUS.ENTERED:
                if (touchStatus === TOUCH_STATUS.OUT) {
                    currentNodeObject.callbacks.onLeave && currentNodeObject.callbacks.onLeave();
                    currentNodeObject.status = NODE_OBJECT_STATUS.IDLE;
                }
                break;
            case NODE_OBJECT_STATUS.IDLE:
                if (touchStatus === TOUCH_STATUS.OVER) {
                    currentNodeObject.callbacks.onDragStart && currentNodeObject.callbacks.onDragStart(currentTouch);
                    currentNodeObject.status = NODE_OBJECT_STATUS.DRAGGING;
                    currentNodeObject.data = {
                        touchId: currentTouch.identifier
                    };
                }
                break;
        }
    }
};

var handleGeneralTouchEnd = function (evt) {
    var touches = evt.changedTouches || [evt];

    for (var i = 0; i < touches.length; i++) {
        var index = getTouchIndexById(touches[i].identifier);
        currentTouches.splice(index, 1);
    }

    if (currentTouches.length === 0) {
        for (var i = 0; i < connectedNodes.length; i += 1) {
            var currentNodeObject = connectedNodes[i];
            switch (currentNodeObject.status) {
                case NODE_OBJECT_STATUS.DRAGGING:
                    connectedNodes[i].status = NODE_OBJECT_STATUS.IDLE;
                    if (connectedNodes[i].data.nodeOver) {
                        connectedNodes[i].data.nodeOver.callbacks.onDropOver && connectedNodes[i].data.nodeOver.callbacks.onDropOver();
                    }

                    if (connectedNodes[i]) {
                        connectedNodes[i].callbacks.onDragEnd && connectedNodes[i].callbacks.onDragEnd();
                        connectedNodes[i].data = null;
                    }
                    break;
                case NODE_OBJECT_STATUS.OVER:
                    connectedNodes[i].status = NODE_OBJECT_STATUS.IDLE;
                    connectedNodes[i].callbacks.onLeave && connectedNodes[i].callbacks.onLeave();
                    break;
            }
            ;
        }
    }
};

var handleGeneralTouchMove = function (evt) {
    var touches = evt.changedTouches || [evt],
        currentNodeObject,
        touchStatus,
        currentTouch;

    for (var j = 0; j < connectedNodes.length; j++) {
        currentNodeObject = connectedNodes[j];
        for (var i = 0; i < currentTouches.length; i++) {
            touchStatus = TOUCH_STATUS.OUT;
            if (isPointInNode(touches[i], currentNodeObject)) {
                if (!(currentNodeObject.status === TOUCH_STATUS.DRAGGING && currentTouches[i].identifier === currentNodeObject.data)) {
                    touchStatus = TOUCH_STATUS.OVER;
                    currentTouch = touches[i];
                }
            }
        }

        switch (currentNodeObject.status) {
            case NODE_OBJECT_STATUS.ENTERED:
                if (touchStatus === TOUCH_STATUS.OUT) {
                    currentNodeObject.callbacks.onLeave && currentNodeObject.callbacks.onLeave();
                    currentNodeObject.status = NODE_OBJECT_STATUS.IDLE;
                }
                break;
            case NODE_OBJECT_STATUS.IDLE:
                if (touchStatus === TOUCH_STATUS.OVER) {
                    currentNodeObject.callbacks.onEnter && currentNodeObject.callbacks.onEnter();
                    currentNodeObject.status = NODE_OBJECT_STATUS.ENTERED;
                }
                break;
            case NODE_OBJECT_STATUS.DRAGGING:
                currentTouch = getTouchById(currentNodeObject.data.touchId, touches);
                currentNodeObject.callbacks.onDrag && currentNodeObject.callbacks.onDrag(currentTouch);

                var nodeOver = isTouchOver(currentTouch, currentNodeObject);
                if (nodeOver) {
                    if (currentNodeObject.data.nodeOver === nodeOver) {
                        // Do nothing we were already over this node
                    } else {
                        if (currentNodeObject.data.nodeOver) {
                            onDragNodeLeave(currentNodeObject, currentNodeObject.data.nodeOver);
                        }

                        onDragNodeEnter(currentNodeObject, nodeOver);
                    }
                } else {
                    onDragNodeLeave(currentNodeObject, currentNodeObject.data.nodeOver);
                }
                break;
        }
    }
};

var Events = function () {
    document.body.addEventListener("touchstart", blockEvent, false);
    document.body.addEventListener("touchmove", blockEvent, false);
    document.body.addEventListener("touchstart", handleGeneralTouchStart, false);
    document.body.addEventListener("touchcancel", handleGeneralTouchEnd, false);
    document.body.addEventListener("touchend", handleGeneralTouchEnd, false);
    document.body.addEventListener("touchmove", handleGeneralTouchMove, false);
    
    document.body.addEventListener("mousedown", handleGeneralTouchStart, false);
    document.body.addEventListener("mousemove", handleGeneralTouchMove, false);
    document.body.addEventListener("mouseup", handleGeneralTouchEnd, false);
};

Events.prototype.setBlockTouch = function (bool) {
    blockingEvents = !!bool;
};

Events.prototype.connectNode = function (node, config) {
    console.log('connectNode');
    connectedNodes.push({
        node: node,
        size: getNodePosition(node),
        status: NODE_OBJECT_STATUS.IDLE,
        callbacks: config
    });
};

Events.prototype.disconnectNode = function (node) {
    var indexToRemove = -1;

    for (var i = 0; i < connectedNodes.length; i += 1) {
        if (connectedNodes[i].node === node) {
            indexToRemove = i;
            break;
        }
    }

    if (indexToRemove != -1) {
        connectedNodes.splice(indexToRemove, 1);
    }
};

Events.prototype.disconnectAll = function () {
    connectedNodes = [];
};

export default new Events();