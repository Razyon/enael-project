function prepareRawSVG(svgText) {
    var containerNode = document.createElement('div');
    containerNode.innerHTML = svgText;

    var svgNode = containerNode.children[0];
    if (!svgNode.hasAttribute('viewBox')) {
        svgNode.setAttribute('viewBox', '0 0 ' + svgNode.getAttribute('width') + ' ' + svgNode.getAttribute('height'));
    }
    svgNode.removeAttribute('width');
    svgNode.removeAttribute('height');

    return svgNode;
}

export default {
    getSVG: function (fileName, callback) {
        var xhr = new XMLHttpRequest();

        xhr.open('GET', fileName);
        xhr.onload = function () {
            callback(prepareRawSVG(xhr.responseText));
        };

        xhr.send();
    }
}