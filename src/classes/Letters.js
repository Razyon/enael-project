'use strict';

var letters = {
  'a': [
    {x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 3, y: 0}, {x: 4, y: 0},
    {x: 0, y: 1},                                           {x: 4, y: 1},
    {x: 0, y: 2}, {x: 1, y: 2}, {x: 2, y: 2}, {x: 3, y: 2}, {x: 4, y: 2},
    {x: 0, y: 3},                                           {x: 4, y: 3},
    {x: 0, y: 4},                                           {x: 4, y: 4}
  ],
  'e': [
    {x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}, {x: 3, y: 0}, {x: 4, y: 0},
    {x: 0, y: 1},
    {x: 0, y: 2}, {x: 1, y: 2}, {x: 2, y: 2}, {x: 3, y: 2},
    {x: 0, y: 3},
    {x: 0, y: 4}, {x: 1, y: 4}, {x: 2, y: 4}, {x: 3, y: 4}, {x: 4, y: 4}
  ],
  'l': [
    {x: 0, y: 0},
    {x: 0, y: 1},
    {x: 0, y: 2},
    {x: 0, y: 3},
    {x: 0, y: 4}, {x: 1, y: 4}, {x: 2, y: 4}, {x: 3, y: 4}, {x: 4, y: 4}
  ],
  'n': [
    {x: 0, y: 0},                                           {x: 4, y: 0},
    {x: 0, y: 1}, {x: 1, y: 1},                             {x: 4, y: 1},
    {x: 0, y: 2},               {x: 2, y: 2},               {x: 4, y: 2},
    {x: 0, y: 3},                             {x: 3, y: 3}, {x: 4, y: 3},
    {x: 0, y: 4},                                           {x: 4, y: 4}
  ],
  ' ': []
};

var Letters = function (config) {
  if (!config) { var config = {}; }
  
  this.pixelClassPrefix = config.pixelClassPrefix || 's';
  this.pixelClassSeparator = config.pixelClassSeparator || 'x';
  this.pixelNodeTag = config.pixelNodeTag || 'div';
  this.pixelNodeClassName = config.pixelNodeClassName || '';
  this.pixelInnerHTML = config.pixelInnerHTML || '';
  this.letterNodeTag = config.letterNodeTag || 'div';
  this.letterNodeClassName = config.letterNodeClassName || '';
};

Letters.prototype.writeString = function (string, container) {
  var realContainer = container || document.body,
      node;
  
  for (var i = 0; i < string.length; i += 1) {
    node = document.createElement(this.letterNodeTag);
    node.className = this.letterNodeClassName;
    
    this.writeLetter(string[i], node);
    
    realContainer.appendChild(node);
  }
};
  
Letters.prototype.writeLetter = function (letter, container) {
  var realContainer = container || document.body;
  var letterInfo = letters[letter];
  var node;
  
  if (letterInfo) {
    for (var i = 0; i < letterInfo.length; i += 1) {
      node = document.createElement(this.pixelNodeTag);
      node.className = this.pixelNodeClassName + ' ' + this.pixelClassPrefix + letterInfo[i].x + this.pixelClassSeparator + letterInfo[i].y;
      node.innerHTML = this.pixelInnerHTML;
      realContainer.appendChild(node);
    }
  } else {
    console.warn('letter writing issue, uknown letter: ', letter);
  }
};

export default Letters;
