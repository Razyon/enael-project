/**
 * Shuffle an array
 */
function shuffle (o) {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {
    ;
    }
    return o;
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary (min, max) {
    return Math.random() * (max - min) + min;
}

var Effects = {
    randomiseNodesToggle: function (querySelector, cssClass) {
        var maxTimeout = 30;
        var minTimeout = 1;
        var toggledNodes = [];
        var index = 0;
        var toggleNodes = function (nodes) {
            if (index < nodes.length) {
                var node = nodes[index];

                var timeout = getRandomArbitrary(minTimeout, maxTimeout);
                setTimeout(function () {
                    toggledNodes.push(node);
                    node.classList.toggle(cssClass);
                    index += 1;
                    toggleNodes(nodes);
                }, timeout);
            }
        }
        var nodes = document.querySelectorAll(querySelector);
        var nodeArray = [];
        for (var i = 0; i < nodes.length; i += 1) {
            nodeArray.push(nodes[i]);
        }

        shuffle(nodeArray);

        toggleNodes(nodeArray);
    }
};

export default Effects;
