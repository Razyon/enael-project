'use strict';

import SVG from './SVG'

var shapes = ['polygon_01', 'polygon_02', 'square', 'star_01', 'star_02', 'star_03', 'triangle_01'];
var colors = ['blue', 'red', 'orange', 'brown', 'green', 'pink'];

function randomize (min, max) {
    return (Math.round((Math.random() * max)) % (max-min+1)) + min;
}

var id = 1;
function getUniqueId () {
    return id++;
}

var Shape = function (domNode, config) {
    var self = this;

    SVG.getSVG('images/shapes/' + config.shape + '.svg', function (svgNode) {
        self.svgNode = svgNode;
        if (config.color) {
            self.addGradient(config.color);
        }
        self.domNode.appendChild(svgNode);
    });

    this.domNode = domNode;
    this.domNode.classList.add('shape');
    this.domNode.classList.add(config.class);
    this.shapeName = config.shape;
};

Shape.prototype.addGradient = function (color) {
    var linearGradientNode;
    var radialGradientNode;
    var xmlns = "http://www.w3.org/2000/svg",
        xlinkns = "http://www.w3.org/1999/xlink";

    if (this.gradient) {
        linearGradientNode = this.svgNode.getElementsByTagNameNS(xmlns, 'linearGradient')[0];
        radialGradientNode = this.svgNode.getElementsByTagNameNS(xmlns, 'radialGradient')[0];

        linearGradientNode.parentNode.removeChild(linearGradientNode);
        radialGradientNode.parentNode.removeChild(radialGradientNode);

        this.gradient = null;
    }

    var radialGradientId = getUniqueId();
    var linearGradientId = getUniqueId();
    radialGradientNode = document.createElementNS(xmlns, 'radialGradient');
    radialGradientNode.setAttributeNS(xlinkns, 'xlink:href', '#linearGradient' + linearGradientId);
    radialGradientNode.setAttributeNS(null, 'id', 'radialGradient' + radialGradientId);
    radialGradientNode.setAttributeNS(null, 'cx', '416.76025');
    radialGradientNode.setAttributeNS(null, 'cy', '643.65717');
    radialGradientNode.setAttributeNS(null, 'fx', '416.76025');
    radialGradientNode.setAttributeNS(null, 'fy', '643.65717');
    radialGradientNode.setAttributeNS(null, 'r', '492.42371');
    radialGradientNode.setAttributeNS(null, 'gradientTransform', 'matrix(2.2095365,-0.95343348,0.88048507,2.0404817,-1061.3003,-253.93647)');
    radialGradientNode.setAttributeNS(null, 'gradientUnits', 'userSpaceOnUse');

    linearGradientNode = document.createElementNS(xmlns, 'linearGradient');
    linearGradientNode.classList.add('gradient-' + color);
    linearGradientNode.setAttributeNS(null, 'id', 'linearGradient' + linearGradientId);
    linearGradientNode.innerHTML = '<stop offset="0"></stop><stop offset="1"></stop>';

    var defs = this.svgNode.getElementsByTagNameNS(xmlns, 'defs')[0];

    console.log(this.svgNode.getElementsByTagNameNS(xmlns, 'path')[0]);

    this.svgNode.getElementsByTagNameNS(xmlns, 'path')[0].setAttributeNS(null, 'style', 'fill:url(#radialGradient' + radialGradientId + ');fill-opacity:1');

    defs.appendChild(linearGradientNode);
    defs.appendChild(radialGradientNode);

    this.gradient = color;
};

Shape.SHAPES = shapes;
Shape.COLORS = colors;
export default Shape;
