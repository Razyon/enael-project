var gulp = require('gulp');
var path = require('path');

var less = require('gulp-less');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var plumber = require('gulp-plumber');

var LessPluginCleanCSS = require('less-plugin-clean-css');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var cleancss = new LessPluginCleanCSS({ advanced: true });
var autoprefix= new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });

var config = {
  app: './app',
  dist: './dist'
}

// Clean tmp folder
gulp.task('clean-all', ['clean-css', 'clean-tmp', 'clean-dist'], function () {
  return gulp.src('.tmp', {read: false})
    .pipe(clean());
});
gulp.task('clean-css', function () {
  return gulp.src('.tmp/styles', {read: false})
    .pipe(clean());
});
gulp.task('clean-dist', function () {
  return gulp.src(config.dist, {read: false})
    .pipe(clean());
});
gulp.task('clean-tmp', function () {
  return gulp.src('.tmp', {read: false})
    .pipe(clean());
});


// Compile less file into css file
gulp.task('less', ['clean-css'], function () {
  return gulp.src(config.app + '/styles/main.less')
    .pipe(plumber({
      errorHandler: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(less({
      plugins: [cleancss, autoprefix]
    }))
    .pipe(gulp.dest('.tmp/styles/'))
    .pipe(connect.reload());
});

gulp.task('usemin', ['clean-all', 'less'], function () {
  return gulp.src(config.app + '/index.html')
    .pipe(usemin({
      css: [],
      html: [],
      js: [uglify()]
    }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('connect', function () {
  connect.server({
    root: ['.tmp', config.app],
    port: 9100,
    livereload: true
  })
});

gulp.task('reload-scripts', function () {
  return gulp.src(config.app + '/scripts/*.js')
    .pipe(connect.reload())
});
gulp.task('reload-html', function () {
  console.log('here');
  return gulp.src(config.app + '/*.html')
    .pipe(connect.reload())
});

// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch(config.app + '/styles/*.less', ['less']);
  gulp.watch(config.app + '/*.html', ['reload-html']);
  gulp.watch(config.app + '/scripts/*.js', ['reload-scripts']);
});

gulp.task('copy', ['clean-all'], function() {
  return gulp.src(config.app + '/**/*.svg')
    .pipe(gulp.dest(config.dist));
});


gulp.task('default', ['less', 'connect', 'watch']);
gulp.task('build', ['usemin', 'copy']);