var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        app: './src/main.js',
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    'style-loader', // creates style nodes from JS strings
                    'css-loader', // translates CSS into CommonJS
                    'less-loader', // compiles Less to CSS
                ]
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            title: 'Development',
        }),
        new CopyPlugin({
            patterns: [
                { from: 'assets', to: 'dist' },
            ],
        }),
    ],
    devServer: {
        contentBase: path.join(__dirname, 'assets'),
        compress: false,
        port: 9000
    }
};