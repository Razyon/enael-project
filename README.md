#Enael-Project
Enal-Project is a compilation of games for children.

It is written in pure Typescript

## Environment
To develop or build the project you need to have NodeJS installed.

Then run the following commands to install the dependencies:
> `npm install -g gulp-cli` // Allow you to use 'grunt' in command line
> `npm install` // Install all the NodeJS dependencies described in package.json

## Build & development
To develop the project we use Grunt as task manager. To run the project in development mode, the command line is:
> `gulp`


This command may not launch the web browser, the project will be available at this url: http://localhost:9100.
It will also launch a "watch" task which will watch over your source files and reload the project when the source changes.

To package a build version use the following command:
> `gulp build`

This will populate the "dist" folder with the built project.

## Configuration
